package main

import (
	cmd "gitlab.com/madpipe/ardei/ardei/cmd"
)

func main() {
	cmd.Execute()
}
