variables:
  GOLANG_VERSION: "1.17.6"
  GORELEASER_VERSION: "1.3.1"

  # When using dind, it's wise to use the overlayfs driver for
  # improved performance.
  DOCKER_DRIVER: overlay2
  # Specify to Docker where to create the certificates, Docker will
  # create them automatically on boot, and will create
  # `/certs/client` that will be shared between the service and
  # build container.
  DOCKER_TLS_CERTDIR: "/certs"
  # Mimic variables set in the docker image entrypoint:
  # https://github.com/docker-library/docker/blob/master/20.10/docker-entrypoint.sh
  # Because we are not using the docker:latest images for the jobs
  DOCKER_CERT_PATH: "$DOCKER_TLS_CERTDIR/client"
  DOCKER_HOST: "tcp://docker:2376"
  DOCKER_TLS_VERIFY: 1

default:
  image: "golang:$GOLANG_VERSION-alpine3.15"

.release: &release
  services:
    - docker:20.10.7-dind
  before_script:
    - |
      echo "https://dl-cdn.alpinelinux.org/alpine/edge/community" >> /etc/apk/repositories

      apk add --no-cache \
        build-base \
        cosign \
        docker-cli \
        git \
        wget

    - |
      cd /tmp
      wget https://github.com/goreleaser/goreleaser/releases/download/v${GORELEASER_VERSION}/goreleaser_${GORELEASER_VERSION}_x86_64.apk
      wget https://github.com/goreleaser/goreleaser/releases/download/v${GORELEASER_VERSION}/checksums.txt
      wget https://github.com/goreleaser/goreleaser/releases/download/v${GORELEASER_VERSION}/checksums.txt.sig
      wget https://github.com/goreleaser/goreleaser/releases/download/v${GORELEASER_VERSION}/checksums.txt.pem

      cosign verify-blob \
        --cert checksums.txt.pem \
        --signature checksums.txt.sig \
        checksums.txt

      grep "goreleaser_${GORELEASER_VERSION}_x86_64.apk" checksums.txt > checksum_only_apk.txt
      sha256sum -c checksum_only_apk.txt

      apk add --allow-untrusted "./goreleaser_${GORELEASER_VERSION}_x86_64.apk"
      goreleaser --version

      cd -

    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY

stages:
  - test
  - release

test:
  stage: test
  before_script:
    - apk add --no-cache
      build-base
      docker-cli
    - go get -u github.com/jstemmer/go-junit-report
  except:
    - tags
  script:
    - go fmt $(go list ./...)
    - go vet $(go list ./...)
    - |
      go test ./... -v -coverprofile=coverage 2>&1 | tee test_output.txt || export FAIL=$?
      cat test_output.txt | go-junit-report -set-exit-code > report.xml
      go tool cover -func=coverage
      exit $FAIL
  artifacts:
    reports:
      junit: report.xml

release-test:
  stage: release
  <<: *release
  only:
    - main
  script:
    - goreleaser release --snapshot --rm-dist
    - |
      docker tag \
          $CI_REGISTRY_IMAGE \
          $CI_REGISTRY_IMAGE:$CI_COMMIT_REF_SLUG
    - echo -e "Pushing docker image \e[1m$CI_REGISTRY_IMAGE:$CI_COMMIT_REF_SLUG\e[0m"
    - docker push $CI_REGISTRY_IMAGE:$CI_COMMIT_REF_SLUG

release:
  stage: release
  <<: *release
  only:
    - tags
  variables:
    # Disable shallow cloning so that goreleaser can diff between tags to
    # generate a changelog.
    GIT_DEPTH: 0
    GITLAB_TOKEN: $CI_JOB_TOKEN
  script:
    - goreleaser release --rm-dist
