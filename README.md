# Ardei

**Ardei** is a monitorig tool that lets you know when your nodes go offline and come back online.

## Configuration

A `config.yaml` file is required in one of these locations:

* `/etc/ardei/`
* `/$HOME/.ardei/`
* `.` - working directory

Sample config

```
hosts:
    - host1
    - host2
telegramToken: "FOOBAR"
telegramChatID: 1234
lockExtension: ".lock" # Optional
checkCron: "*/5 * * * *" # Optional
```

All keys can also be provided as environment variables in the format `ARDEI_<KEY>` for a more secure deployment. For example:

```
ARDEI_TELEGRAMTOKEN="ABCXXX"
```

## Usage

```
./ardei
```
