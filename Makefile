SHELL:=/bin/bash

release-test:
	export CI_PROJECT_NAME=ardei; \
	export CI_PROJECT_NAMESPACE=madpipe; \
	export CI_SERVER_HOST=gitlab.com; \
	export CI_PROJECT_PATH="$$CI_PROJECT_NAMESPACE/$$CI_PROJECT_NAME"; \
	export CI_REGISTRY_IMAGE="registry.gitlab.com/$$CI_PROJECT_PATH"; \
	\
	goreleaser release --snapshot --skip-publish --rm-dist

docker:
	docker run --rm -d --name ardei-nginx-test -t -p 80:80 nginx:alpine

stop-docker:
	docker stop ardei-nginx-test

run:
	./dist/ardei_linux_amd64/ardei

test:
	$(MAKE) release-test
	export ARDEI_CHECKCRON="*/1 * * * *"; \
	$(MAKE) run &
	sleep 70
	$(MAKE) docker
	sleep 70
	$(MAKE) stop-docker
	pkill ardei
