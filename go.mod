module gitlab.com/madpipe/ardei

go 1.16

require (
	github.com/nikoksr/notify v0.17.3
	github.com/robfig/cron/v3 v3.0.0
	github.com/spf13/cobra v1.2.1
	github.com/spf13/viper v1.9.0
)
