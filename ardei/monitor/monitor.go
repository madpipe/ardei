package monitor

import (
	"fmt"
	"log"
	"net/http"
)

func IsServerUp(server string) bool {
	log.Printf("Checking server %s", server)

	url := fmt.Sprintf("http://%s", server)
	resp, err := http.Get(url)
	if err != nil {
		log.Print(err.Error())
		return false
	}
	defer resp.Body.Close()
	if resp.StatusCode != 200 {
		return false
	}

	return true
}
