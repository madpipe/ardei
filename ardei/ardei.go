package ardei

import (
	"fmt"
	"log"
	"time"

	"github.com/robfig/cron/v3"
	"gitlab.com/madpipe/ardei/ardei/alerts"
	"gitlab.com/madpipe/ardei/ardei/config"
	"gitlab.com/madpipe/ardei/ardei/monitor"
)

// Execute the app business logic
func Execute() {

	config.LoadConfig()
	alerts.Setup()

	c := cron.New()
	c.AddFunc(config.GetConfigString(config.ConfigCheckCron), check)
	c.Start()
	defer c.Stop()

	// Keep alive for cron to run
	for true {
		time.Sleep(5 * time.Minute)
	}
}

func check() {
	for _, server := range config.GetHosts() {
		if !monitor.IsServerUp(server) {
			log.Printf("Server %s is DOWN\n", server)
			if !alerts.NotificationSentLocked(server) {
				alerts.LockNotificationSent(server, true)
				message := fmt.Sprintf("🛑 Server \"%s\" is DOWN!", server)
				alerts.SendNotification("Offline warning", message)
			} else {
				log.Println("Notification already sent")
			}
		} else {
			if alerts.NotificationSentLocked(server) {
				log.Printf("Server %s is back UP", server)
				alerts.LockNotificationSent(server, false)
				message := fmt.Sprintf("✅ Server \"%s\" is back UP!", server)
				alerts.SendNotification("Offline warning", message)
			}
		}
	}
}
