package alerts

import (
	"context"
	"fmt"
	"log"
	"os"
	"time"

	"github.com/nikoksr/notify"
	"github.com/nikoksr/notify/service/telegram"
	"gitlab.com/madpipe/ardei/ardei/config"
)

var notifier *notify.Notify

// Setup prepares the alerting services
func Setup() {
	setupTelegramBot()
}

func setupTelegramBot() {
	telegramService, err := telegram.New(config.GetConfigString(config.ConfigTelegramToken))
	if err != nil {
		log.Printf("Error registering with the Telegram service:\n%s\n", err.Error())
	}
	telegramService.AddReceivers(config.GetConfigInt64(config.ConfigTelegramChatID))
	notifier = notify.New()
	notifier.UseServices(telegramService)
}

// SendNotification to the configured destination with the provided message
func SendNotification(subject, message string) {
	err := notifier.Send(
		context.Background(),
		subject,
		message,
	)
	if err != nil {
		log.Printf("Error sending notification:\n%s\n", err.Error())
	}
}

func getLockFilename(hostname string) string {
	return fmt.Sprintf("%s%s", hostname, config.GetConfigString(config.ConfigLockExtension))
}

// LockNotificationSent toggles the lock-file that keeps track if a notification was sent
func LockNotificationSent(hostname string, lock bool) {
	lockFile := getLockFilename(hostname)

	if lock {
		f, err := os.Create(lockFile)
		if err != nil {
			log.Panic(err.Error())
		}
		defer f.Close()
		f.WriteString(time.Now().String())
	} else {
		err := os.Remove(lockFile)
		if err != nil {
			log.Panic(err.Error())
		}
	}
}

// NotificationSentLocked validates wether the notification was already sent
func NotificationSentLocked(hostname string) bool {
	lockFile := getLockFilename(hostname)
	if _, err := os.Stat(lockFile); err == nil {
		return true
	}
	return false
}
