package cmd

import (
	"fmt"
	"os"

	"gitlab.com/madpipe/ardei/ardei"

	"github.com/spf13/cobra"
	"gitlab.com/madpipe/ardei/ardei/build"
)

var (
	rootCmd = &cobra.Command{
		Use: build.ProjectName,
		Run: func(cmd *cobra.Command, args []string) {
			ardei.Execute()
		},
	}

	versionCmd = &cobra.Command{
		Use:   "version",
		Short: "Print the version number",
		Run: func(cmd *cobra.Command, args []string) {
			buildStr := fmt.Sprintf("%s.%s.%s", build.VersionMajor, build.VersionMinor, build.VersionPatch)
			fmt.Println(buildStr)
		},
	}
)

// Execute executes the root command.
func Execute() error {
	if err := rootCmd.Execute(); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	return nil
}

func init() {
	rootCmd.AddCommand(versionCmd)
}
