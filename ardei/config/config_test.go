package config

import (
	"fmt"
	"reflect"
	"strings"
	"testing"

	"github.com/spf13/viper"
)

func Test_checkConfig(t *testing.T) {
	tests := []struct {
		name   string
		err    bool
		errMsg string
		config map[string]string
	}{
		{
			name: "unset",
			err:  true,
		},
		{
			name:   ConfigTelegramToken,
			err:    false,
			errMsg: "Telegram token not provided\n",
			config: map[string]string{
				ConfigTelegramToken: "ABCD",
			},
		},
		{
			name:   ConfigTelegramChatID,
			err:    false,
			errMsg: "Telegram ChatID not provided\n",
			config: map[string]string{
				ConfigTelegramChatID: "ABCD",
			},
		},
		{
			name:   configHosts,
			err:    false,
			errMsg: "Hosts list not provided\n",
			config: map[string]string{
				configHosts: "ABCD",
			},
		},
		{
			name: "allSet",
			err:  false,
			config: map[string]string{
				ConfigTelegramToken:  "ABCD",
				ConfigTelegramChatID: "ABCD",
				configHosts:          "ABCD",
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			viper.Reset()
			for k, v := range tt.config {
				viper.Set(k, v)
			}
			err := checkConfig()
			if err != nil && !tt.err {
				if len(tt.errMsg) == 0 || strings.Contains(err.Error(), tt.errMsg) {
					fmt.Println(err)
					t.Fail()
				}
			}
		})
	}
}

func TestGetHosts(t *testing.T) {
	hostsList := []string{
		"localhost",
		"example.com",
	}
	tests := []struct {
		name string
		want []string
	}{
		{
			name: "HostsUnset",
			want: nil,
		},
		{
			name: "HostsSet",
			want: hostsList,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			viper.Reset()
			if tt.want != nil {
				viper.Set(configHosts, tt.want)
			}
			if got := GetHosts(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("GetHosts() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestGetConfigString(t *testing.T) {
	type args struct {
		configKey string
	}
	tests := []struct {
		name string
		args args
		want string
	}{
		{
			name: "ValidKey",
			args: args{
				configKey: "testKey",
			},
			want: "testValue",
		},
		{
			name: "UnsetKey",
			args: args{
				configKey: "unsetKey",
			},
			want: "",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			viper.Reset()
			if tt.want != "" {
				viper.Set(tt.args.configKey, tt.want)
			}
			if got := GetConfigString(tt.args.configKey); got != tt.want {
				t.Errorf("GetConfigString() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestGetConfigInt64(t *testing.T) {
	type args struct {
		configKey string
	}
	tests := []struct {
		name string
		args args
		want int64
	}{
		{
			name: "ValidKey",
			args: args{
				configKey: "testKey",
			},
			want: 123,
		},
		{
			name: "UnsetKey",
			args: args{
				configKey: "unsetKey",
			},
			want: 0,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			viper.Reset()
			if tt.want != 0 {
				viper.Set(tt.args.configKey, tt.want)
			}
			if got := GetConfigInt64(tt.args.configKey); got != tt.want {
				t.Errorf("GetConfigInt64() = %v, want %v", got, tt.want)
			}
		})
	}
}
