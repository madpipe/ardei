package config

import (
	"errors"
	"log"

	"github.com/spf13/viper"
	"gitlab.com/madpipe/ardei/ardei/build"
)

// Configuration keys
const (
	ConfigTelegramToken  = "telegramToken"
	ConfigTelegramChatID = "telegramChatID"
	configHosts          = "hosts"
	ConfigLockExtension  = "lockExtension"
	ConfigCheckCron      = "checkCron"
)

// LoadConfig initializes and validates the config
func LoadConfig() {
	setupConfig()
	err := checkConfig()
	if err != nil {
		log.Fatalln(err.Error())
	}
}

func setupConfig() {
	// Default configuration valies
	viper.SetDefault(ConfigLockExtension, ".lock")
	viper.SetDefault(ConfigCheckCron, "*/5 * * * *")

	// Configuration file
	viper.SetConfigName("config")
	viper.SetConfigType("yaml")
	viper.AddConfigPath("/etc/ardei/")
	viper.AddConfigPath("$HOME/.ardei/")
	viper.AddConfigPath(".")
	err := viper.ReadInConfig()
	if err != nil {
		log.Fatalf("Error reding config file: %s \n", err.Error())
	}

	// Configuration environment variables
	viper.SetEnvPrefix(build.ProjectName)
	viper.AutomaticEnv()
}

func checkConfig() error {
	msg := ""
	if !viper.IsSet(ConfigTelegramToken) {
		msg = msg + "Telegram token not provided\n"
	}
	if !viper.IsSet(ConfigTelegramChatID) {
		msg = msg + "Telegram ChatID not provided\n"
	}
	if !viper.IsSet(configHosts) {
		msg = msg + "Hosts list not provided\n"
	}
	if len(msg) > 0 {
		return errors.New(msg)
	}
	return nil
}

// GetHosts returns the list of hosts to scan
func GetHosts() []string {
	return viper.GetStringSlice(configHosts)
}

// GetConfigString returns the requested string config
func GetConfigString(configKey string) string {
	return viper.GetString(configKey)
}

// GetConfigInt64 returns the requested int64 config
func GetConfigInt64(configKey string) int64 {
	return viper.GetInt64(configKey)
}
